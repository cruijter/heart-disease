#!/usr/bin/env python3

"""
Reading a file and combining
mutiple lines to one line
and writing this to a new file.
"""

import sys

__author__ = "Carlijn Ruijter"

def read_file(path_to_file):
    """ Function for reading in the file and
     storing the lines in a list.
     One record consisted of 10 lines, these
     were added together to get them on the same line."""

    # read the file put the lines of the file in a list
    lines = [line.strip() for line in open(path_to_file)]

    # join the blocks of 10 lines together on the same line
    data = [" ".join(lines[i:i+10]) for i in range(0, len(lines), 10)]
    return data

def write_data(data, filename):
    """ Writing the cleaned data to a new file. """

    # making the new file
    new_file = open("cleaned_"+filename+".data", "w")

    # writing the data to the new file
    new_data = [new_file.write(line + "\n") for line in data]

def main(args):
    file_path = args[1]
    data = read_file(file_path)
    new_data = write_data(data, file_path[:-5])
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
