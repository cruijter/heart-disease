This project was done to find out which attributes are most important for determining the diagnosis of coronary artery disease. This repository only contains information about how this project is done and one python file to process some data.

The heart-disease-log.Rmd file contains the log where the process, and all the steps done for the project are described. The heart-disease-log.pdf is the log in pdf format. 

The exploratory-data-analysis Rmarkdown file contains a report about the exploratory data analysis done for this project. A preamable-latex.tex file is used in the Rmd to give the report the correct layout of figures and explantion of the figures.

The codeboox.txt file is a self-made codebook for the project. It contains the information about all the attributes.

For this project a reference project was used. The article for this previous research is the file heart-disease.pdf.

The data folder contains all the data used in the process for this project. The cleveland.data and hungarian.data are the data files directly from the reference project. The cleaned_cleveland.data and cleaned_hungarian.data are processed through a written python file to make the files readable in R. The python script used for this is reading_file.py.

The heart_disease.arff file contains the data of the cleveland patients in the arff format to be used in Weka. The hungarian_data.arff is the arff format of the data from Hungary.

The folder quality_metrics contains the files with the accuracy, speed, number of true positives, false positives, true negatives and false negatives for different algorithms. The file quality_metrics.txt show these features for 8 different algorithms. cost_sensitive_classifier.txt contains these features for the algorithms when the cost sensitive classifier was used in Weka. The quality_metrics_attributes.txt contains the features for 4 algorithms when the attribute selected classifier was performed on the data.

The folder attributes_selected contains the output from the attributes selected classifier for the 4 algorithms, corresponding to the name of the files. The folder select_att_results has the data for only the selected attributes per algorithm.