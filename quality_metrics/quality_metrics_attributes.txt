algorithm, ACC(%), TP, FP, TN, FN
NaiveBayes, 83.3333, 94, 16, 141, 31
SimpleLogistic, 81.5603, 98, 25, 132, 27
SMO, 75.1773, 87, 32, 125, 38
RandomForest, 78.7234, 93, 28, 129, 32
